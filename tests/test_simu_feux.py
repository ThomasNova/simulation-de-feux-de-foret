import unittest

from simu_feux import nombreVoisinsEnFeu, miseAJourEtatCase, calculFormule


class TestSimulateurDeFeux(unittest.TestCase):
    def test_nombreVoisinsEnFeu(self):
        tableau = []
        for _ in range(0, 10):
            sous_tableau = []
            tableau.append(sous_tableau)
            for _ in range(0, 10):
                sous_tableau.append(0)
        # ligne.colonne
        tableau[5][5] = 2
        tableau[5][4] = 2
        tableau[5][6] = 2
        tableau[4][5] = 2
        tableau[1][1] = 2

        self.assertEqual(nombreVoisinsEnFeu(tableau, 5, 5), 3)
        self.assertEqual(nombreVoisinsEnFeu(tableau, 1, 1), 0)

    def test_miseAJourEtatCase(self):
        self.assertEqual(miseAJourEtatCase(0), 0)
        self.assertEqual(miseAJourEtatCase(1), 2)
        self.assertEqual(miseAJourEtatCase(3), 0)

    def test_calculFormule(self):
        self.assertEqual(calculFormule(1), 0.5)
        self.assertEqual(calculFormule(0), 0)
        self.assertEqual(calculFormule(3), 0.75)
