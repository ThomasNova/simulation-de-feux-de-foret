import random


def creationTableau(nb_ligne, nb_colonne, probabilite):
    '''
    Crée un tableau de deux dimensions en ajoutant des arbres aléatoirement
    en fonction de la probabilité de boisement

    :param nb_ligne: nombre de lignes du tableau
    :type nb_ligne: int
    :param nb_colonne: nombre de colonnes du tableau
    :type nb_colonne: int
    :param probabilite: probabilité de boisement
    :type probabilite: float
    :return: tableau représentant le nombre de lignes et le nombre de colonnes
    :rtype: list
    '''
    tableau = []
    for _ in range(0, nb_ligne):
        sous_tableau = []
        tableau.append(sous_tableau)
        for _ in range(0, nb_colonne):
            if(random.random() <= probabilite):
                sous_tableau.append(1)
            else:
                sous_tableau.append(0)
    return tableau


def nombreVoisinsEnFeu(tableau, ligne, colonne):
    '''
    Calcule le nombre de voisins qui sont en feu pour une case du tableau

    :param tableau: tableau représentant le nombre de lignes et le nombre de colonnes
    :type tableau: list
    :param ligne: ligne indiquant la case
    :type ligne: int
    :param colonne: colonne indiquant la case
    :type colonne: int
    :return: nombre de voisins en feu pour une case
    :rtype: int
    '''
    nb_voisin = 0
    if((ligne != len(tableau)-1) and (tableau[ligne+1][colonne] == 2)):
        nb_voisin += 1
    if((ligne != 0) and (tableau[ligne-1][colonne] == 2)):
        nb_voisin += 1
    if((colonne != len(tableau[ligne])-1) and (tableau[ligne][colonne+1] == 2)):
        nb_voisin += 1
    if((colonne != 0) and (tableau[ligne][colonne-1] == 2)):
        nb_voisin += 1
    return nb_voisin


def miseAJourEtatCase(valeur_case):
    '''
    Met à jour l'état d'une case du tableau

    :param valeur_case: état de la case du tableau (0, 1, 2 ou 3)
    :type valeur_case: int
    :return: état de la case à l'état suivant
    :rtype: int
    '''
    if(valeur_case == 3):
        valeur_case = 0
    elif(valeur_case != 0):
        valeur_case += 1
    return valeur_case


def calculFormule(k):
    '''
    Calucle la formule pour la règle 2
    :param k: nombre de voisins en feu
    :type k: int
    :return: résultat de la formule
    :rtype: float
    '''
    return 1-1/(k+1)
