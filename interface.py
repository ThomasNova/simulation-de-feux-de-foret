'''
    The `Simulation de feux de forêt` module
    ========================================

    Un simulateur de feux de forêt où l'utilisateur peut placer des foyers de feux
    dans une forêt avec différents modules de Python (argparse, parser, random, tkinter,...).
'''


import argparse
import parser
import random
import time
from tkinter import Canvas, Tk, PhotoImage
from tkinter.ttk import Button, Label
from simu_feux import *


def initialisation_carte():
    '''
    Initialise la carte avec des états vides et des arbres

    :return: grille à maillage carré (état vide ou arbre)
    :rtype: list
    '''
    carte = []
    for row in range(0, args.rows):
        carte.append([])
        for column in range(0, args.cols):
            if(tableau[row][column] == 0):
                color = 'white'
            elif(tableau[row][column] == 1):
                color = 'green'
            carte[row].append(canvas.create_rectangle(
                column*args.cell_size, row*args.cell_size,
                column*args.cell_size+args.cell_size,
                row*args.cell_size+args.cell_size, fill=color))
    return carte


def click_fire(event):
    '''
    Modifie l'état d'une case en feu à l'aide de l'événement clique gauche de la souris

    :param event: événement clique gauche de la souris
    :type event: event
    '''
    x = int(event.x / args.cell_size)
    y = int(event.y / args.cell_size)
    if(tableau[y][x] == 1):
        tableau[y][x] = 2
        color = 'red'
        canvas.itemconfig(carte[y][x], fill=color)
        to_update.append({'row': y, 'col': x})


def click_arbre(event):
    '''
    Annule le choix d'une case en feu et modifie l'état d'une case en arbre 
    à l'aide de l'événement clique droit de la souris    

    :param event: événement clique droit de la souris
    :type event: event
    '''
    x = int(event.x / args.cell_size)
    y = int(event.y / args.cell_size)
    if(tableau[y][x] == 2):
        tableau[y][x] = 1
        color = 'green'
        canvas.itemconfig(carte[y][x], fill=color)
        to_update.remove({'row': y, 'col': x})


def callback_simulation_feux():
    '''
    Appel au lancement de la simulation de feux de forêt avec les deux règles mises en place
    '''
    while(to_update != []):
        # on copie la liste qui sera itérée afin qu'elle reste fixe
        # pendant une itération de la boucle for
        to_update_loop = to_update.copy()
        for element in to_update_loop:
            if(args.rule == 1):
                if((element['row'] != args.rows-1) and (tableau[element['row']+1][element['col']] == 1)):
                    to_update.append(
                        {'row': element['row']+1, 'col': element['col']})
                    tableau[element['row']+1][element['col']] = 2
                if((element['row'] != 0) and (tableau[element['row']-1][element['col']] == 1)):
                    to_update.append(
                        {'row': element['row']-1, 'col': element['col']})
                    tableau[element['row']-1][element['col']] = 2
                if((element['col'] != args.cols-1) and (tableau[element['row']][element['col']+1] == 1)):
                    to_update.append(
                        {'row': element['row'], 'col': element['col']+1})
                    tableau[element['row']][element['col']+1] = 2
                if((element['col'] != 0) and (tableau[element['row']][element['col']-1] == 1)):
                    to_update.append(
                        {'row': element['row'], 'col': element['col']-1})
                    tableau[element['row']][element['col']-1] = 2
            else:
                if((element['row'] != args.rows-1) and (tableau[element['row']+1][element['col']] == 1)
                   and (random.random() <= calculFormule(nombreVoisinsEnFeu(tableau, element['row']+1, element['col'])))):
                    to_update.append(
                        {'row': element['row']+1, 'col': element['col']})
                    tableau[element['row']+1][element['col']] = 2
                if((element['row'] != 0) and (tableau[element['row']-1][element['col']] == 1)
                   and (random.random() <= calculFormule(nombreVoisinsEnFeu(tableau, element['row']-1, element['col'])))):
                    to_update.append(
                        {'row': element['row']-1, 'col': element['col']})
                    tableau[element['row']-1][element['col']] = 2
                if((element['col'] != args.cols-1) and (tableau[element['row']][element['col']+1] == 1)
                   and (random.random() <= calculFormule(nombreVoisinsEnFeu(tableau, element['row'], element['col']+1)))):
                    to_update.append(
                        {'row': element['row'], 'col': element['col']+1})
                    tableau[element['row']][element['col']+1] = 2
                if((element['col'] != 0) and (tableau[element['row']][element['col']-1] == 1)
                   and (random.random() <= calculFormule(nombreVoisinsEnFeu(tableau, element['row'], element['col']-1)))):
                    to_update.append(
                        {'row': element['row'], 'col': element['col']-1})
                    tableau[element['row']][element['col']-1] = 2
            if(tableau[element['row']][element['col']] == 2):
                canvas.itemconfig(carte[element['row']]
                                  [element['col']], fill='red')
            if(tableau[element['row']][element['col']] == 3):
                canvas.itemconfig(carte[element['row']]
                                  [element['col']], fill='grey')
            if(tableau[element['row']][element['col']] == 0):
                canvas.itemconfig(carte[element['row']]
                                  [element['col']], fill='white')
            if(tableau[element['row']][element['col']] == 0):
                to_update.remove(element)
            tableau[element['row']][element['col']] = miseAJourEtatCase(
                tableau[element['row']][element['col']])
        master.update()
        time.sleep(args.sleep)


if __name__ == '__main__':
    '''
    Application de la simulation de feux de forêt

    Mise en place des arguments en ligne de commande (nombre de lignes et de colonnes, 
    taille des cellules, probabilité de boisement, la règle à choisir et
    le temps entre deux étapes).
    Vérification des arguments passés en ligne de commande
    Création et configuration de la fenêtre
    Mise en place d'une grille
    Ajout d'éléments dans l'interface (Canvas, Bouton et Labels)
    '''

    parser = argparse.ArgumentParser('Une simulation de feux de forêt.')
    parser.add_argument(
        '-rows', help='Le nombre de lignes.', type=int, default=10)
    parser.add_argument(
        '-cols', help='Le nombre de colonnes.', type=int, default=10)
    parser.add_argument(
        '-cell_size', help='La taille de chaque cellule.', type=int, default=35)
    parser.add_argument(
        '-afforestation', help='Le pourcentage de boisement.', type=float, default=0.5)
    parser.add_argument(
        '-rule', help='La règle pour la propagation du feu.', type=int, default=1)
    parser.add_argument(
        '-sleep', help='Le temps de pause entre deux étapes.', type=float, default=2)
    args = parser.parse_args()
    if(args.rows <= 0):
        parser.error('Argument rows invalide !')
    if(args.cols <= 0):
        parser.error('Argument cols invalide !')
    if(args.cell_size <= 0):
        parser.error('Argument cell_size invalide !')
    if(args.afforestation <= 0):
        parser.error('Argument afforestation invalide !')
    if(args.rule != 1 and args.rule != 2):
        parser.error('Argument rule invalide !')
    if(args.sleep < 0):
        parser.error('Argument sleep invalide !')

    tableau = creationTableau(args.rows, args.cols, args.afforestation)

    master = Tk()
    master.title("Simulation de Feux de Forêt")
    icon = PhotoImage(file="icon.png")
    master.iconphoto(False, icon)

    master.rowconfigure(0, weight=1)
    master.rowconfigure(1, weight=1)
    master.rowconfigure(2, weight=1)
    master.columnconfigure(0, weight=1)
    master.columnconfigure(1, weight=1)

    canvas_width = args.cols*args.cell_size
    canvas_height = args.rows*args.cell_size
    canvas = Canvas(master, width=canvas_width, height=canvas_height)
    canvas.grid(row=0, columnspan=2, padx=10, pady=10)
    canvas.pack
    carte = initialisation_carte()

    to_update = []

    canvas.bind('<Button-1>', click_fire)
    canvas.bind('<Button-3>', click_arbre)

    button_start = Button(master, text='Lancer la simulation',
                          command=callback_simulation_feux)
    button_start.grid(row=1, rowspan=2, column=0)

    label_clique_gauche = Label(
        master, text='Mettre le feu : clique gauche de la souris')
    label_clique_gauche.grid(row=1, column=1)

    label_clique_droit = Label(
        master, text='Annuler le feu : clique droit de la souris')
    label_clique_droit.grid(row=2, column=1)

    master.resizable(width=False, height=False)

    master.mainloop()
